echo "Cloning Repo and writing commit messages to a text file."
git clone https://bitbucket.org/calanceus/api-test
cd api-test
git log --pretty=format:"%ci %s %n" master > ../commit_messages.txt